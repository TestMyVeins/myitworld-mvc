<?php

namespace app\models;

use app\core\UserModel;

class User extends UserModel
{
  const STATUS_INACTIVE = 0;
  const STATUS_ACTIVE = 1;
  const STATUS_DELETED = 2;

  public string $login = '';
  public string $email = '';
  public int $status = self::STATUS_INACTIVE;
  public string $password = '';
  public string $confirmPassword = '';

  public function tableName(): string
  {
    return 'users';
  }

  public function primaryKey(): string{
    return 'id';
  }

  public function save(){
    $this->password = password_hash($this->password, PASSWORD_BCRYPT);
    return parent::save();
  }

  public function rules(): array{
      return [
        'login' => [self::RULE_REQUIRED],
        'email' => [self::RULE_REQUIRED, self::RULE_EMAIL, [
          self::RULE_UNIQUE, 'class' => self::class
        ]],
        'password' => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 8], [self::RULE_MAX, 'max' => 24]],
        'confirmPassword' => [self::RULE_REQUIRED, [self::RULE_MATCH, 'match' => 'password']],
      ];
  }

  public function attributes(): array
  {
    return ['login', 'email', 'password', 'status'];
  }

  public function labels(): array{
    return [
      'login' => 'Login',
      'email' => 'Email',
      'password' => 'Hasło',
      'confirmPassword' => 'Powtórz hasło'
    ];
  }

  public function getDisplayName(): string{
    return $this->login;
  }

}
