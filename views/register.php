<?php

/** @var $model \app\model\User */

?>
<h1>Register</h1>
<?php $form = \app\core\form\Form::begin('', "post")?>  
  <?= $form->field($model, 'login') ?>
  <?= $form->field($model, 'email') ?>
  <?= $form->field($model, 'password')->passwordField() ?>
  <?= $form->field($model, 'confirmPassword')->passwordField() ?>

  <button type="submit" class="btn btn-primary">Submit</button>

  
<?= \app\core\form\Form::end()?>  