<!doctype html>
<html lang='pl'>
<head>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <title>MyItWorld - Strona główna</title>
  <meta name='description' content='Jesteś osobą ambitną i ciekawą świata? Dołącz do naszej społeczności, zapraszamy!'>

  <meta property='og:title' content='MyItWorld - Strona główna'>
  <meta property='og:type' content='website'>
  <meta property='og:url' content='https://www.myitworld.pl'>
  <meta property='og:description' content='Jesteś osobą ambitną i ciekawą świata? Dołącz do naszej społeczności, zapraszamy!'>
  <meta property='og:image' content='og-image.png'>
  <meta property='og:site_name' content='MyItWorld' />
  <meta property='og:locale' content='pl_PL'>
  <meta property='fb:admin' content='862506317144806'>
  <meta property='fb:app_id' content='511831713138644'>

  <link rel='icon' href='../assets/img/utility/favicon.png'>
  <link rel='apple-touch-icon' href='../assets/img/utility/favicon.png'>

  <!-- <link rel='stylesheet' href='css/styles.css?v=1.0'> -->
</head>
<body>
{{content}}
</body>
</html