<?php

namespace app\core;

class Application {

  public static string $ROOT_DIR;

  public string $userClass;
  public Router $router;
  public Request $request;
  public Response $response;
  public Session $session;
  public Database $db;
  public ?DbModel $user;
 
  public static Application $app;
  public Controller $controller;
  public function __construct($rootPath, array $config){
    $this->userClass = $config['userClass'];
    self::$ROOT_DIR = $rootPath;
    self::$app = $this;
    session_unset();
    $this->request = new Request();
    $this->response = new Response();
    $this->session = new Session();
    $this->router = new Router($this->request, $this->response);

    $this->db = new Database($config['db']);

    $primaryValue = $this->session->get('user');
    if($primaryValue){
      $userModel = new $config['userClass'];
      $primaryKey = $userModel->primaryKey();
      $this->user = $userModel->findOne([$primaryKey => $primaryValue]);
    } else {
      $this->user = NULL;
    }
  }

  public function run(){
    echo $this->router->resolve();
  }
  public function getController(){
    return $this->controller;
  }
  
  public function setController($controller){
    return $this->controller = $controller;
  }

  public function login(DbModel $user){
    $this->user = $user;
    $primaryKey = $user->primaryKey();
    $primaryValue = $user->{$primaryKey};
    $this->session->set('user', $primaryValue);
    return true;
  }

  public function logout(){
    $this->user = null;
    $this->session->remove('user');
  }

  public static function isGuest(){
    return !self::$app->user;
  }

}
?>
