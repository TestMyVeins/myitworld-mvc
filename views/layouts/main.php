<?php
use app\core\Application;

// echo '<pre>';
// var_dump(Application::$app->user->getDisplayName());
// echo '</pre>';

// echo '<pre>';
// var_dump(__DIR__);
// echo '</pre>';



?>
<!doctype html>
<html lang='pl'>
<head>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <title>MyItWorld - Strona główna</title>
  <meta name='description' content='Jesteś osobą ambitną i ciekawą świata? Dołącz do naszej społeczności, zapraszamy!'>

  <meta property='og:title' content='MyItWorld - Strona główna'>
  <meta property='og:type' content='website'>
  <meta property='og:url' content='https://www.myitworld.pl'>
  <meta property='og:description' content='Jesteś osobą ambitną i ciekawą świata? Dołącz do naszej społeczności, zapraszamy!'>
  <meta property='og:image' content='og-image.png'>
  <meta property='og:site_name' content='MyItWorld' />
  <meta property='og:locale' content='pl_PL'>
  <meta property='fb:admin' content='862506317144806'>
  <meta property='fb:app_id' content='511831713138644'>

  <link rel='icon' href='../assets/images/favicon.png'>
  <link rel='apple-touch-icon' href='../assets/images/favicon.png'>

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;900&family=Merriweather:wght@400;700&family=Pacifico&display=swap" rel="stylesheet">


  <link rel='stylesheet' href='stylesheets/mobile.css'>
  <link rel='stylesheet' media='screen and (min-width: 800px)' href='stylesheets/desktop.css'>
  <link rel='stylesheet' href='stylesheets/icons.css'>
</head>
<body>
<header id='mainMenu'>
  <a href='/' id='logo'>
    <svg width="50" height="50"><path fill="#060606" stroke="#000" d="M8 25L1 41s0 0 0 0h48s0 0 0 0l-6-16H8z"/><path fill="#2C2C2C" stroke="#1B1B1B" stroke-width=".2" d="M7 34l3-7h31l2 7H7zm0 0s0 0 0 0l3-7h31l2 7s0 0 0 0H7z"/><path fill="#060606" stroke="#000" d="M42 25h1a129225 129225 0 011-20H7L6 6l2 19s0 0 0 0h34zM8 7h35l-2 17H9L8 7z"/><path fill="#1B1B1B" d="M9 26a346 346 0 004-1H9v1zM38 26v-1h3v1h-3z"/><path fill="#131313" stroke="#1F1F1F" stroke-width=".2" d="M21 35v4h8l1-1v-3h-1-8z"/></svg>
    MyItWorld
  </a>
  <input type='checkbox' class='d-none' id='navToggle'>
  <nav>
    <div id='navList'>
      <a href=''>
        <div>
          <span>Blog</span>
        </div>
      </a>
      <a href=''>
        <div>
          <span>Regulamin</span>
        </div>
      </a>
    </div>
  </nav>
  <label for='navToggle' class='navLabel'><i class='i-list icon s2'></i></label>
</header>


  <?php if(Application::$app->session->getFlash('success')): ?>
  <div class='alert alert-succes'>
    <?= Application::$app->session->getFlash('success') ?>
  </div>
  <?php endif; ?>
{{content}}
</body>
</html